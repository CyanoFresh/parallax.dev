var debugInput, updateDebugState;

debugInput = document.querySelector('input');

updateDebugState = function() {
  document.body.classList.toggle('debug-on', debugInput.checked);
}

debugInput.addEventListener('click', updateDebugState);
